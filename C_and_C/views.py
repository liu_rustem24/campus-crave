from django.db import transaction
from django.db.models import Q, Count
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from C_and_C.forms import SearchForm, SortForm, ProductForm, RequestForm, CategoryForm, ShopForm
from C_and_C.models import Shop, Product, Category, Order, OrderItem, Request
from accounts.models import CustomUser


# Create your views here.

class ShopCreateView(CreateView):
    model = Shop
    form_class = ShopForm
    template_name = 'shop/create_shop.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        has_approved_request = self.request.user.requests.filter(topic="Shop Creation", approved=True,
                                                                 used=False).exists()
        has_refused_request = self.request.user.requests.filter(topic="Shop Creation", refused=True)
        context['has_approved_request'] = has_approved_request
        context['has_refused_request'] = has_refused_request
        return context

    def form_valid(self, form):
        if self.request.user.requests.filter(topic='Shop Creation', approved=True).exists():
            form.instance.owner = self.request.user
            request_obj = Request.objects.get(user=self.request.user, topic='Shop Creation', approved=True, used=False)
            request_obj.used = True
            request_obj.save()
            return super().form_valid(form)
        else:
            print('error')

    def get_success_url(self):
        return reverse('shops')


class ShopListView(ListView):
    model = Shop
    context_object_name = 'shops'
    template_name = 'main-page/index.html'
    search_form = SearchForm
    search_value = None

    def get(self, request, *args, **kwargs):
        self.search_form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        pk = request.POST.get('product_pk')
        print(f"product primary key is {pk}")
        selected_products = request.session.get('selected_products', [])
        print(selected_products)
        if pk:
            selected_products.append(pk)
            request.session['selected_products'] = selected_products
            request.session.modified = True  # Mark the session as modified
        return redirect('shops')

    def get_search_form(self):
        return self.search_form(self.request.GET)

    def get_search_value(self):
        if self.search_form.is_valid():
            return self.search_form.cleaned_data.get('search')
        return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        if self.search_value:
            query = Q(name__icontains=self.search_value)
            context['products'] = context['products'].filter(query)
        return context


class ShopDetailView(DetailView):
    model = Shop
    context_object_name = 'shop'
    template_name = 'shop/shop.html'
    search_value = None
    search_form = SearchForm
    sort_form = SortForm
    sort_value = None

    def get(self, request, *args, **kwargs):
        self.search_form = self.get_search_form()
        self.search_value = self.get_search_value()

        if 'reset_selected_products' in request.GET:
            request.session.pop('selected_products', None)
            request.session.modified = True
        return super().get(request, *args, **kwargs)

    def get_search_form(self):
        return self.search_form(self.request.GET)

    def get_search_value(self):
        if self.search_form.is_valid():
            return self.search_form.cleaned_data.get('search')
        return None

    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id')
        user_is_seller_mode = request.POST.get('is_seller_mode')
        print(f'User id is {user_id} In the seller mode: {user_is_seller_mode}')

        if user_id and user_is_seller_mode == 'false':
            user = CustomUser.objects.get(pk=user_id)
            user.is_seller_mode = True
            user.save()
        elif user_id and user_is_seller_mode == 'true':
            user = CustomUser.objects.get(pk=user_id)
            user.is_seller_mode = False
            user.save()
        pk = request.POST.get('product_pk')
        # print(f"product primary key is {pk}")
        selected_products = request.session.get('selected_products', [])
        # print(selected_products)
        if pk and pk not in selected_products:
            selected_products.append(pk)
            request.session['selected_products'] = selected_products
            request.session.modified = True  # Mark the session as modified
        return redirect(reverse('shop_detail', kwargs={'pk': kwargs['pk']}))

    def get_context_data(self, *args, **kwargs):
        sort_by = self.request.GET.get('sort_by')
        context = super().get_context_data(**kwargs)
        context['products'] = Product.objects.filter(shop=self.object)
        context['search_form'] = self.search_form
        context['categories'] = Category.objects.all()
        context['selected_products'] = self.request.session.get('selected_products', [])
        selected_product_pks = self.request.session.get('selected_product_pks', [])
        if self.search_value:
            query = Q(name__icontains=self.search_value)
            context['products'] = context['products'].filter(query)
        if sort_by:
            query = Q(category__name__icontains=sort_by)
            context['products'] = context['products'].filter(query)
        if 'reset_selected_products' in self.request.GET:
            self.request.session.pop('selected_product_pks', None)
            self.request.session.pop('selected_products', None)

        return context


class ShopDeleteView(DeleteView):
    model = Shop
    template_name = 'shop/delete_shop.html'
    context_object_name = 'shop'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        has_approved_request = self.request.user.requests.filter(topic="Shop Delete", approved=True,
                                                                 used=False).exists()
        context['shop'] = self.object
        context['has_approved_request'] = has_approved_request
        return context

    def form_valid(self, form):
        if self.request.user.requests.filter(topic='Shop Delete', approved=True).exists():
            request_obj = Request.objects.get(user=self.request.user, topic='Shop Delete', approved=True, used=False)
            request_obj.used = True
            request_obj.save()
            return super().form_valid(form)
        else:
            print('error')

    def get_success_url(self):
        return reverse('shops')


class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'product/product.html'


class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'product/create_product.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        shop_id = self.kwargs['pk']  # Assuming the shop_id is passed through the URL kwargs
        shop = get_object_or_404(Shop, pk=shop_id)
        form.instance.shop = shop
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('shop_detail', kwargs={'pk': self.kwargs['pk']})


class ProductDeleteView(DeleteView):
    model = Product
    context_object_name = 'product'
    template_name = 'product/delete_product.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        shop_pk = self.request.POST.get('shop_pk')
        return reverse('shop_detail', kwargs={'pk': shop_pk})


class ProductUpdateView(UpdateView):
    model = Product
    form_class = ProductForm
    context_object_name = 'product'
    template_name = 'product/update_product.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        shop_pk = self.request.POST.get('shop_pk')
        return reverse('shop_detail', kwargs={'pk': shop_pk})


def buy_product(request, shop_pk):
    pks = request.GET.getlist('pks')
    quantities = request.GET.getlist('quantities')
    shop = get_object_or_404(Shop, pk=shop_pk)
    categories = Category.objects.all()
    products = Product.objects.filter(shop=shop)
    errors = []

    if not pks:
        errors.append("No products selected.")
        return render(request, 'shop/shop.html',
                      {'errors': errors, 'products': products, 'categories': categories, 'shop': shop})

    products = Product.objects.filter(pk__in=pks)
    print(f"Found {len(products)} products")
    print(f'products: {products}')
    print(f'Quantities are {quantities}')

    if not products:
        errors.append("No products found for the selected IDs.")
        return render(request, 'shop/shop.html',
                      {'errors': errors, 'products': products, 'categories': categories, 'shop': shop})

    total_cost = 0

    order = None

    for product, quantity in zip(products, quantities):
        if request.user == product.shop.owner:
            errors.append("It is your product!\n If you want to buy it go to the customer mode")
            break

        if product.number and product.number >= int(quantity) > 0:
            if order is None:
                order = Order.objects.create(user=request.user)

            OrderItem.objects.create(order=order, product=product, shop=product.shop, quantity=int(quantity))
            product.number -= int(quantity)
            total_cost += product.cost * int(quantity)
            product.save()
        elif product.number < int(quantity):
            errors.append(f"We don't have enough {product.name} in stock.")
            break

    if order is not None:
        order.total = total_cost
        order.save()
    else:
        errors.append("Failed to create the order due to errors.")

    if errors:
        return render(request, 'shop/shop.html',
                      {'errors': errors, 'products': products, 'categories': categories, 'shop': shop})

    return redirect('order_list')


def purchase_product(request, pk):
    selected_products = []
    selected_product = get_object_or_404(Product, pk=pk)
    shop = selected_product.shop
    selected_products.append(selected_product)
    context = {
        'selected_products': selected_products,
        'shop': shop,
        'products': Product.objects.filter(shop=shop),
    }
    return render(request, 'shop/shop.html', context=context)


class OrderListView(ListView):
    model = Order
    template_name = 'orders/my_order.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        user = self.request.user
        context = super().get_context_data(**kwargs)
        context['orders'] = Order.objects.filter(user=self.request.user)
        context['waiting_orders'] = Order.objects.filter(user=self.request.user, is_ready=False)
        context['done_orders'] = Order.objects.filter(user=self.request.user, is_ready=True)
        context['orders_for_shop'] = Order.objects.filter(orderitem__shop__owner=self.request.user, is_ready=False)
        context['orders_for_shop_done'] = Order.objects.filter(orderitem__shop__owner=self.request.user, is_ready=True)
        return context

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            order_pk = request.POST.get('order_pk')
            new_status = request.POST.get('new_status')
            order = get_object_or_404(Order, pk=order_pk)

            if request.user == order.orderitem_set.first().shop.owner:
                if new_status == 'ready':
                    order.is_ready = True
                elif new_status == 'not_ready':
                    order.is_ready = False
                order.save()

        return redirect('order_list')


class RequestCreateView(CreateView):
    model = Request
    form_class = RequestForm
    template_name = 'requests/create_request.html'
    shop_id = None

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')

        shop_id = self.request.GET.get('shop')
        self.shop_id = shop_id
        print(shop_id)
        print(self.shop_id)

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        print(self.shop_id)
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('request_list')


class RequestListView(ListView):
    model = Request
    template_name = 'requests/requests.html'

    def post(self, request, *args, **kwargs):
        if 'approve_request_id' in request.POST:
            request_id = request.POST.get('approve_request_id')
            request_obj = Request.objects.get(id=request_id)
            request_obj.approved = True
            request_obj.refused = False
            if request_obj.topic == 'Is Seller':
                request_obj.user.is_seller = True
                request_obj.user.save()
            request_obj.save()
            return HttpResponseRedirect(reverse('request_list'))
        elif 'refuse_request_id' in request.POST:
            request_id = request.POST.get('refuse_request_id')
            request_obj = Request.objects.get(id=request_id)
            request_obj.refused = True
            request_obj.approved = False
            request_obj.save()
            return HttpResponseRedirect(reverse('request_list'))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_superuser:
            context['requests'] = Request.objects.all()
        else:
            context['requests'] = Request.objects.filter(user=self.request.user)

        return context


class RequestUpdateView(UpdateView):
    model = Request
    form_class = RequestForm
    template_name = 'requests/update_request.html'
    context_object_name = 'request'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('request_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context


class RequestDeleteView(DeleteView):
    model = Request
    template_name = 'requests/delete_request.html'
    context_object_name = 'request'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('request_list')


class CategoryCreateView(CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'category/create_category.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('category_list')


class CategoryListView(ListView):
    model = Category
    template_name = 'category/list_category.html'
    context_object_name = 'categories'
