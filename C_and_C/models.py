from django.db import models
from django.contrib.auth import get_user_model


# Create your models here.


class Shop(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    location = models.CharField(max_length=100, null=False, blank=False)
    owner = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, null=False, blank=False, related_name='shops')
    image = models.ImageField(upload_to='shop_images/', null=True, blank=True)

    def __str__(self):
        return f"Shop: {self.name}"


class Category(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class Product(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE , null=False, blank=False, related_name='products')
    cost = models.IntegerField(null=False, blank=False)
    ingredients = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to='product_images/', null=True, blank=True)
    number = models.IntegerField(null=True, blank=True, default=1)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=False, related_name='products')

    def __str__(self):
        return f"{self.name} Ingredients: {self.ingredients}"


class Order(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, null=False, blank=False, related_name='orders')
    date = models.DateTimeField(auto_now_add=True)
    total = models.IntegerField(null=True, blank=True)
    is_ready = models.BooleanField(default=False)
    in_the_process = models.BooleanField(default=False)

    def __str__(self):
        return f'Order - {self.id}'


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    shop = models.ForeignKey(Shop, on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f"Order: {self.order}, Product: {self.product}, Shop: {self.shop}, Quantity: {self.quantity}"


class Request(models.Model):
    SHOP_CREATION = 'Shop Creation'
    SHOP_DELETE = 'Shop Delete'
    IS_SELLER = 'Is Seller'
    OTHER_TOPIC = 'Other Topic'

    TOPIC_CHOICES = [
        (SHOP_CREATION, 'Shop Creation'),
        (SHOP_DELETE, 'Shop Delete'),
        (IS_SELLER, 'Is Seller'),
    ]
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, null=False, blank=False,
                             related_name='requests')
    topic = models.CharField(max_length=100, choices=TOPIC_CHOICES, default=SHOP_CREATION)
    description = models.TextField(null=True, blank=True)
    document = models.FileField(upload_to='request_documents/', null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)
    refused = models.BooleanField(default=False)
    used = models.BooleanField(default=False)
    shop = models.ForeignKey(Shop, on_delete=models.SET_NULL, null=True, blank=True, related_name='requests')

