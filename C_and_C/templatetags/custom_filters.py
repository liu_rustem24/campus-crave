from django import template  # Import your Product model

from C_and_C.models import Product

register = template.Library()


@register.filter(name='get_product')
def get_product(pk):
    try:
        return Product.objects.get(pk=pk)
    except Product.DoesNotExist:
        return None
