from django.contrib import admin

from C_and_C.models import Shop, Product, Category, Order, OrderItem, Request


class OrderAdmin(admin.ModelAdmin):
    list_display = ('shop', 'category', 'date', 'user', 'product', 'total')


admin.site.register(Shop)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(Request)

# Register your models here.
