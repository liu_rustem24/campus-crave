from django.urls import path, include
from .views import *

urlpatterns = [
    path('', ShopListView.as_view(), name='shops'),
    path('shop/<int:pk>/', ShopDetailView.as_view(), name='shop_detail'),
    path('shop/product/<int:pk>/', ProductDetailView.as_view(), name='product_list'),
    path('shop/<int:shop_pk>/product/buy/', buy_product, name='buy_product'),
    path('order/my_orders/', OrderListView.as_view(), name='order_list'),
    path('shop/create_new/', ShopCreateView.as_view(), name='shop_create'),
    path('shop/delete/<int:pk>', ShopDeleteView.as_view(), name='shop_delete'),
    path('shop/<int:pk>/product/create_new/', ProductCreateView.as_view(), name='product_create'),
    path('shop/product/delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),
    path('shop/product/update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('category/list/', CategoryListView.as_view(), name='category_list'),
    path('category/create_new/', CategoryCreateView.as_view(), name='category_create'),
    path('request/create_new/', RequestCreateView.as_view(), name='request_create'),
    path('requests/requests/', RequestListView.as_view(), name='request_list'),
    path('requests/delete/<int:pk>', RequestDeleteView.as_view(), name='request_delete'),
    path('requests/update/<int:pk>', RequestUpdateView.as_view(), name='request_update'),
    path('requests/admin_mode_requests/', RequestListView.as_view(), name='admin_mode_requests'),
    # path('shop/product/<int:pk>/select', purchase_product, name='purchase_product'),
]