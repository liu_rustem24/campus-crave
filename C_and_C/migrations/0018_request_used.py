# Generated by Django 5.0.4 on 2024-05-06 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('C_and_C', '0017_request'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='used',
            field=models.BooleanField(default=False),
        ),
    ]
