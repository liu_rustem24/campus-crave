from django import forms

from C_and_C.models import Order, OrderItem, Shop, Request, Product, Category


class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, label='search')


class SortForm(forms.Form):
    sort = forms.CharField(max_length=100, label='sort')


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'


class OrderItemForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = '__all__'
        error_messages = {
            'product': {
                'invalid': "Invalid product selected.",
            },
            'quantity': {
                'required': "Please specify a quantity.",
                'invalid': "Invalid quantity value.",
            },
        }

        def clean(self):
            cleaned_data = super().clean()
            order = cleaned_data.get('order')
            product = cleaned_data.get('product')
            quantity = cleaned_data.get('quantity')

            if order and product and quantity:
                if product.number == 0 or product.number is None:
                    raise forms.ValidationError("We run out of products")
                if quantity > product.number:
                    raise forms.ValidationError("Quantity exceeds available stock")

            return cleaned_data


class ShopForm(forms.ModelForm):
    class Meta:
        model = Shop
        exclude = ['owner']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control w-50'}),
            'location': forms.TextInput(attrs={'class': 'form-control w-50'}),
            'image': forms.FileInput(attrs={'class': 'form-control w-50'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get('name')
        location = cleaned_data.get('location')

        if not name or not location:
            raise forms.ValidationError("You should specify a name and location.")
        if len(location) < 3:
            raise forms.ValidationError("You should specify a location and give more information about location.")

        return cleaned_data


class RequestForm(forms.ModelForm):
    class Meta:
        model = Request
        exclude = ['user', 'approved', 'used', 'shop', 'refused']

        widgets = {
            'topic': forms.Select(attrs={'class': 'form-control w-50'}),
            'description': forms.Textarea(attrs={'class': 'form-control w-50'}),
            'document': forms.FileInput(attrs={'class': 'form-control w-50'}),
            'shop': forms.Select(attrs={'class': 'form-control w-50'}),
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ['shop']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control w-50'}),
            'cost': forms.TextInput(attrs={'class': 'form-control w-50'}),
            'ingredients': forms.Textarea(attrs={'class': 'form-control w-50'}),
            'number': forms.NumberInput(attrs={'class': 'form-control w-50'}),
            'image': forms.FileInput(attrs={'class': 'form-control w-50'}),
            'category': forms.Select(attrs={'class': 'form-control w-50'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        cost = cleaned_data.get('cost')
        number = cleaned_data.get('number')

        if not cost or not number:
            raise forms.ValidationError("You should specify a cost and number of products")
        if number < 0 or cost < 0:
            raise forms.ValidationError("You should specify a correct cost and number of products")
        return cleaned_data


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control w-50'}),
            'description': forms.Textarea(attrs={'class': 'form-control w-50'}),
        }

    def clean_name(self):
        name = self.cleaned_data['name']
        if Category.objects.filter(name=name).exists():
            raise forms.ValidationError("A category with this name already exists.")
        return name

    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get('name')
        if not name:
            raise forms.ValidationError("You should specify a name.")
        return cleaned_data
