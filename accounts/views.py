from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from C_and_C.models import Request
from .forms import *
from .models import Profile


# Create your views here.

@csrf_exempt
def login_view(request):
    context = {}
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            next = request.POST.get('next')
            if next:
                return redirect(next)
            return redirect('shops')
        else:
            context['message'] = "Invalid username or password"

    return render(request, 'accounts/login.html', context=context)


# @csrf_exempt
@login_required
def logout_view(request):
    logout(request)
    return redirect('shops')


class RegisterView(generic.CreateView):
    model = get_user_model()
    template_name = 'accounts/signup.html'
    form_class = RegisterForm

    def form_valid(self, form):
        user = form.save()

        if form.cleaned_data.get('position') == 'seller' and form.cleaned_data.get('seller_file'):
            request = Request.objects.create(user=user,topic='Is Seller', document=form.cleaned_data.get('seller_file'))
            print(request)

        Profile.objects.create(user=user)
        login(self.request, user)
        return redirect(self.get_success_url())

    def get_success_url(self):
        return 'shops'


class ProfileView(generic.DetailView):
    model = get_user_model()
    template_name = 'accounts/profile.html'
    context_object_name = 'user_obj'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

    def get_object(self, queryset=None):
        return self.request.user


class ProfileUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = get_user_model()
    template_name = 'accounts/profile_edit.html'
    context_object_name = 'user_object'
    form_class = UserUpdateForm

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
            # print(kwargs)
        return super().get_context_data(**kwargs)

    def get_profile_form(self):
        profile_form = {'instance': self.object.profile}
        if self.request.method == 'POST':
            profile_form['data'] = self.request.POST
            profile_form['files'] = self.request.FILES
        return ProfileUpdateForm(**profile_form)

    def get_success_url(self):
        return reverse('profile')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()

        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return render(context)

    def get_object(self, queryset=None):
        return self.request.user


class PasswordChangeView(LoginRequiredMixin, generic.UpdateView):
    model = get_user_model()
    form_class = PasswordChangeForm
    template_name = 'accounts/change_password.html'
    context_object_name = 'user_obj'

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('login')
