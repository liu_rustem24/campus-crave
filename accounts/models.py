from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.


class CustomUser(AbstractUser):
    GENDER_CHOICES = (
        ('male', 'Male'),
        ('female', 'Female'),
    )
    POSITION_CHOICES = (
        ('student', 'Student'),
        ('worker', 'Worker'),
        ('seller', 'Seller')
    )

    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    position = models.CharField(max_length=20, choices=POSITION_CHOICES)
    seller_file = models.FileField(upload_to='vendor_files/', blank=True)
    is_seller = models.BooleanField(default=False)
    is_seller_mode = models.BooleanField(default=False)

    def __str__(self):
        return self.username


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='profile', null=False,
                                blank=False)
    bio = models.TextField(null=True, blank=True, default="No Bio")
    number = models.CharField(max_length=20, null=True, blank=True)
    avatar = models.ImageField(null=True, blank=True, upload_to='avatars/')
    birth_date = models.DateField(null=True, blank=True, verbose_name='Date of Birth')

    def __str__(self):
        return f'{self.user.get_full_name()}'
