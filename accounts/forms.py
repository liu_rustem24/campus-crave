from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from accounts.models import Profile, CustomUser


class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label='Password', required=True, strip=False)
    password_confirm = forms.CharField(widget=forms.PasswordInput(), label='Confirm Password', required=True,
                                       strip=False)
    seller_file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': False}), required=False)

    STATUS_CHOICES = (
        ('student', 'Student'),
        ('worker', 'Worker'),
        ('seller', 'Seller')
    )
    GENDER_CHOICES = (
        ('male', 'Male'),
        ('female', 'Female'),
    )
    position = forms.ChoiceField(choices=STATUS_CHOICES, widget=forms.RadioSelect(), label='Position', required=False)
    gender = forms.ChoiceField(widget=forms.RadioSelect(), label='Gender', choices=GENDER_CHOICES, required=False)

    def clean(self):
        cleaned_data = super().clean()
        # username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('password_confirm')
        position = cleaned_data.get('position')
        seller_file = cleaned_data.get('seller_file')

        if position == 'seller':
            if not seller_file:
                raise forms.ValidationError("Seller position requires uploading a confirmation file.")

        if position == 'seller' and password == password_confirm and seller_file:
            pass

        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Passwords must match')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user

    class Meta:
        model = CustomUser
        fields = (
            'username', 'password', 'password_confirm',
            'first_name', 'last_name', 'email', 'position', 'seller_file', 'gender'
        )


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'email')
        labels = {'email': 'Email', 'first_name': 'First Name', 'last_name': 'Last Name'}

        widgets = {'email': forms.EmailInput(attrs={'class': 'form-control w-50'}),
                   'first_name': forms.TextInput(attrs={'class': 'form-control w-50'}),
                   'last_name': forms.TextInput(attrs={'class': 'form-control w-50'}), }


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ('user',)
        labels = {'bio': 'Bio', 'avatar': 'Avatar', 'number': 'Number', 'birth_date': 'Birth date'}
        widgets = {'bio': forms.Textarea(attrs={'class': 'form-control w-50', 'rows': 3}),
                   'avatar': forms.FileInput(attrs={'class': 'form-control w-50'}),
                   'number': forms.NumberInput(attrs={'class': 'form-control w-50'}),
                   'birth_date': forms.DateInput(attrs={'class': 'form-control w-50', 'type': 'date'})}


class PasswordChangeForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label='Password',
                               strip=False)
    password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                       label='Password Confirm', strip=False)
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label='Old password',
                                   strip=False)

    def clean_password_confirm(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")

        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError("Password don't match")

        return password_confirm

    def clean_old_password(self):
        old_password = self.cleaned_data.get("old_password")

        if not self.instance.check_password(old_password):
            raise forms.ValidationError("Old password is incorrect")

        return old_password

    def save(self, commit=True):
        user = self.instance
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('old_password', 'password', 'password_confirm')
