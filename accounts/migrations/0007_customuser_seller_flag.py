# Generated by Django 5.0.4 on 2024-04-28 09:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_remove_customuser_vendor_file_customuser_seller_file_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='seller_flag',
            field=models.BooleanField(default=False),
        ),
    ]
