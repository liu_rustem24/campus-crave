# Generated by Django 5.0.4 on 2024-04-19 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_alter_customuser_vendor_file_alter_profile_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='vendor_file',
            field=models.FileField(null=True, upload_to='vendor_files/'),
        ),
    ]
