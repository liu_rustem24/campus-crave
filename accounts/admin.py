from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser


# Register your models here.

class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'profile', 'email', 'gender', 'position', 'seller_file',
        'bio', 'number', 'birth_date', 'is_seller')
    list_filter = ('position', 'is_staff', 'is_superuser', 'groups')
    search_fields = ('username', 'email', 'first_name', 'last_name')
    ordering = ('username',)
    filter_horizontal = ('groups', 'user_permissions',)

    def gender(self, obj):
        return obj.profile.gender

    def seller_file(self, obj):
        return obj.profile.seller_file

    def bio(self, obj):
        return obj.profile.bio

    def number(self, obj):
        return obj.profile.number

    def birth_date(self, obj):
        return obj.profile.birth_date

    def is_seller(self, obj):
        return obj.profile.is_seller

    gender.short_description = 'Gender'
    seller_file.short_description = 'Vendor File'
    bio.short_description = 'Bio'
    number.short_description = 'Number'
    birth_date.short_description = 'Birth Date'


admin.site.register(CustomUser, CustomUserAdmin)
